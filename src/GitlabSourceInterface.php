<?php

namespace Drupal\project_browser_gitlab;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a Gitlab source entity.
 */
interface GitlabSourceInterface extends ConfigEntityInterface {

}

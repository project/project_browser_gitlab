<?php

namespace Drupal\project_browser_gitlab\Plugin\ProjectBrowserSource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\project_browser\Plugin\ProjectBrowserSourceBase;
use Drupal\project_browser\ProjectBrowser\Project;
use Drupal\project_browser\ProjectBrowser\ProjectsResultsPage;
use Drupal\project_browser\ProjectType;
use Drupal\project_browser_gitlab\Entity\GitlabSource;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Project Browser Source Plugin for Gitlab.
 *
 * @ProjectBrowserSource(
 *   id = "project_browser_gitlab",
 *   label = @Translation("Gitlab"),
 *   description = @Translation("Source plugin for Project Browser to search Gitlab."),
 *   deriver = "Drupal\project_browser_gitlab\Plugin\Derivative\GitlabDeriver",
 * )
 */
class Gitlab extends ProjectBrowserSourceBase implements ContainerFactoryPluginInterface {

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected ClientInterface $httpClient;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The Gitlab source entity.
   *
   * @var \Drupal\project_browser_gitlab\Entity\GitlabSource
   */
  protected GitlabSource $source;

  /**
   * Constructor for the plugin.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *  The entity type manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $httpClient, EntityTypeManagerInterface $entity_type_manager, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $httpClient;
    $this->logger = $logger;
    $this->source = $entity_type_manager->getStorage('project_browser_gitlab_source')->load($plugin_definition['id']);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
      $container->get('entity_type.manager'),
      $container->get('logger.factory')->get('project_browser'),
    );
  }


  /**
   * {@inheritdoc}
   * @param array $query
   */
  public function getProjects(array $query = []): ProjectsResultsPage {
    $projects = [];

    // Generate the endpoint path based on the plugin configuration and the
    // query parameters.
    $endpoint = '';
    if ($this->source->group()) {
      $endpoint .= '/groups/' . $this->source->group();
    }
    $endpoint .= '/projects';
    $options['query'] = [
      'per_page' => $query['limit'] ?? 12,
      'page' => $query['page'] + 1 ?? 1,
    ];
    if (!empty($query['categories'])) {
      $options['query']['topic'] = $query['categories'];
    }
    if (!empty($query['search'])) {
      $options['query']['search'] = $query['search'];
    }
    if (!empty($query['sort'])) {
      switch ($query['sort']) {
        case 'a_z':
          $options['query']['order_by'] = 'name';
          $options['query']['sort'] = 'asc';
          break;

        case 'z_a':
          $options['query']['order_by'] = 'name';
          break;

        case 'updated':
          $options['query']['order_by'] = 'updated_at';
          break;
      }
    }

    if (!empty($query['machine_name'])) {
      // The machine_name cannot contain a slash, it was replaced by a pipe.
      // Here we do the reverse operation and encode the machine_name name so it
      // can be used in the API call.
      $options = [];
      $endpoint = '/projects/' . urlencode(str_replace('|', '/', $query['machine_name']));
    }

    $response = $this->doRequest($endpoint, $options);

    // If a machine name is provided, the response is an object, not an array.
    if (!empty($query['machine_name']) && !empty($response['data'])) {
      $response['data'] = [$response['data']];
    }

    foreach ($response['data'] ?? [] as $project) {
      $logo = [
        'file' => [
          'uri' => $project['avatar_url'],
          'resource' => 'image',
        ],
        'alt' => (string) $this->t('@name logo', [
          '@name' => $project['name'],
        ]),
      ];

      $categories = [];
      foreach ($project['topics'] as $index => $topic) {
        $categories[$index] = [
          'id' => $index,
          'name' => $topic,
        ];
      }

      $projects[] = new Project(
        logo: $logo,
        isCompatible: TRUE,
        isCovered: TRUE,
        projectUsageTotal: 0,
        machineName: str_replace('/', '|', $project['path_with_namespace']),
        body: $project['description'] ? ['value' => $project['description']] : [],
        title: $project['name'] ?? '',
        author: [],
        packageName: $project['path_with_namespace'],
        categories: $categories,
        type: ProjectType::Module,
      );
    }

    // In case a query returns 10k+ results, there is no X-Total header.
    $count = 10000;
    if ($response['headers']['X-Total'][0] ?? FALSE) {
      $count = $response['headers']['X-Total'][0];
    }
    elseif (count($projects) < $query['limit']) {
      $count = count($projects);
    }

    return new ProjectsResultsPage($count, array_values($projects), (string) $this->getPluginDefinition()['label'], $this->getPluginId());
  }

  /**
   * {@inheritdoc}
   */
  public function getCategories(): array {
    $endpoint = '/topics';
    $response = $this->doRequest($endpoint);

    $categories = [];
    foreach ($response['data'] ?? [] as $category) {
      $categories[$category['name']] = [
        'id' => $category['name'],
        'name' => $category['name'],
      ];
    }

    return array_values($categories);
  }

  /**
   * A helper function to invoke the Gitlab REST API.
   *
   * @param string $endpoint
   *   The endpoint to invoke.
   * @param string $method
   *   The HTTP method to use.
   *
   * @return array|array[]
   */
  private function doRequest(string $endpoint, array $options = [], string $method = 'GET'): array {
    $endpoint = $this->source->domain() . '/api/v4' . $endpoint;

    $headers = [];
    $headers['Content-Type'] = 'application/json';
    if ($this->source->token()) {
      $headers['Authorization'] = 'Bearer ' . $this->source->token();
    }

    $options['headers'] = $options['headers'] ?? [] + $headers;

    try {
      $response = $this->httpClient->request(
        $method,
        $endpoint,
        $options
      );

      return [
        'data' => json_decode($response->getBody(), TRUE),
        'headers' => $response->getHeaders(),
      ];
    }
    catch (GuzzleException $e) {
      $this->logger->error('Error fetching projects from Gitlab: @error', ['@error' => $e->getMessage()]);
      return [
        'data' => [],
        'headers' => [],
      ];
    }
  }

  /**
   * Returns the available sort options.
   *
   * @return array
   *   Options offered.
   */
  public function getSortOptions(): array {
    return [
      'created' => [
        'id' => 'created',
        'text' => $this->t('Newest First'),
      ],
      'a_z' => [
        'id' => 'a_z',
        'text' => $this->t('A-Z'),
      ],
      'z_a' => [
        'id' => 'z_a',
        'text' => $this->t('Z-A'),
      ],
      'updated' => [
        'id' => 'updated',
        'text' => $this->t('Latest Updated First'),
      ],
    ];
  }

}

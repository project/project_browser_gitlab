<?php

namespace Drupal\project_browser_gitlab\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\project_browser\Plugin\ProjectBrowserSourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class GitlabSourceForm extends EntityForm {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The project browser source manager.
   *
   * @var \Drupal\project_browser\Plugin\ProjectBrowserSourceManager
   */
  protected $manager;

  /**
   * Constructor.
   */
  public function __construct(
    LanguageManagerInterface $language_manager,
    FormBuilderInterface $formBuilder,
    ProjectBrowserSourceManager $manager
  ) {
    $this->languageManager = $language_manager;
    $this->formBuilder = $formBuilder;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(LanguageManagerInterface::class),
      $container->get(FormBuilderInterface::class),
      $container->get(ProjectBrowserSourceManager::class),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Source name'),
      '#default_value' => $this->entity->label(),
      '#placeholder' => 'Drupal.org',
      '#required' => TRUE,
      '#description' => $this->t('Enter a label for this source.'),
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#title' => $this->t('Machine name'),
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'source' => ['label'],
        'exists' => [$this, 'gitlabSourceExists'],
      ],
    ];

    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $this->entity->domain(),
      '#placeholder' => 'https://git.drupalcode.org',
      '#required' => TRUE,
      '#description' => $this->t('The domain of the Gitlab instance.'),
    ];

    $form['token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Bearer token'),
      '#default_value' => $this->entity->token(),
      '#placeholder' => 'SpmWmro1UwxjKnBJrKiG',
      '#description' => $this->t('The @token to authenticate with the Gitlab API.', [
        '@token' => Link::fromTextAndUrl(
          $this->t('bearer token'),
          Url::fromUri('https://docs.gitlab.com/ee/api/rest/index.html#personalprojectgroup-access-tokens')
        )->toString(),
      ]),
    ];

    $form['group'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Group'),
      '#default_value' => $this->entity->group(),
      '#placeholder' => 'project',
      '#description' => $this->t('The @group to filter projects by. Leave empty to show all projects of the Gitlab instance.', [
        '@group' => Link::fromTextAndUrl(
          $this->t('group'),
          Url::fromUri('https://docs.gitlab.com/ee/user/group/')
        )->toString(),
      ]),
    ];

    return $form;
  }

  /**
   * Check to validate that the Gitlab source name does not already exist.
   *
   * @param string $name
   *   The machine name of the context to validate.
   *
   * @return bool
   *   TRUE on context name already exist, FALSE on context name not exist.
   */
  public function gitlabSourceExists($name) {
    $entity = $this->entityTypeManager->getStorage('project_browser_gitlab_source')->loadByProperties(['name' => $name]);

    return (bool) $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    //@todo: Validate the domain.
    //@todo: Validate the connexion.
    //@todo: Validate the group.
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $formState) {
    $status = parent::save($form, $formState);

    if ($status) {
      $this->messenger()->addMessage($this->t('The Gitlab source %label has been added. You need to enable this source into %link.', [
        '%label' => $this->entity->label(),
        '%link' => Link::createFromRoute($this->t('the Project browser settings'), 'project_browser.settings')->toString(),
      ]));
    }
    else {
      $this->messenger()->addMessage($this->t('The Gitlab source was not saved.'));
    }

    $this->manager->clearCachedDefinitions();

    $formState->setRedirect('entity.project_browser_gitlab_source.collection');
  }

}

<?php

namespace Drupal\project_browser_gitlab\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\project_browser\Plugin\ProjectBrowserSourceManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to delete a Gitlab source.
 */
class GitlabSourceDeleteForm extends EntityConfirmFormBase {

  /**
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * @var \Drupal\project_browser\Plugin\ProjectBrowserSourceManager
   */
  protected $manager;

  /**
   * Class constructor.
   */
  public function __construct(MessengerInterface $messenger, ProjectBrowserSourceManager $manager) {
    $this->messenger = $messenger;
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(MessengerInterface::class),
      $container->get(ProjectBrowserSourceManager::class)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the %label source?', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.project_browser_gitlab_source.collection');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();

    $this->messenger->addMessage(
      $this->t('The @label source has been deleted.',
        [
          '@label' => $this->entity->label(),
        ]
      )
    );

    $this->manager->clearCachedDefinitions();

    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}

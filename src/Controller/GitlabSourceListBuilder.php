<?php

namespace Drupal\project_browser_gitlab\Controller;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;

/**
 * Provides a listing of Gitlab sources.
 */
class GitlabSourceListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'project_browser_gitlab_source_list';
  }

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['label'] = $this->t('Source name');
    $header['id'] = $this->t('Machine name');
    $header['domain'] = $this->t('Domain');
    $header['group'] = $this->t('Group');

    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    $row['label'] = $entity->label();
    $row['id'] = $entity->id();
    $row['domain'] = $entity->domain();
    $row['group'] = $entity->group() ?: '-';

    return $row + parent::buildRow($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function getOperations(EntityInterface $entity) {
    $operations = parent::getOperations($entity);

    $operations['edit']['url'] = new Url('entity.project_browser_gitlab_source.edit_form', ['project_browser_gitlab_source' => $entity->id()]);

    // @TODO: Check how the route is built.
    unset($operations['translate']);

    return $operations;
  }

}

<?php

namespace Drupal\project_browser_gitlab\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\project_browser_gitlab\GitlabSourceInterface;

/**
 * Defines a Gitlab Source configuration entity class.
 *
 * @ConfigEntityType(
 *   id = "project_browser_gitlab_source",
 *   label = @Translation("Gitlab source"),
 *   label_singular = @Translation("Gitlab source"),
 *   label_plural = @Translation("Gitlab sources"),
 *   label_count = @PluralTranslation(
 *     singular = @Translation("gitlab source"),
 *     plural = @Translation("gitlab sources"),
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\project_browser_gitlab\Controller\GitlabSourceListBuilder",
 *     "form" = {
 *       "add" = "Drupal\project_browser_gitlab\Form\GitlabSourceForm",
 *       "edit" = "Drupal\project_browser_gitlab\Form\GitlabSourceForm",
 *       "delete" = "Drupal\project_browser_gitlab\Form\GitlabSourceDeleteForm",
 *     },
 *   },
 *   config_prefix = "project_browser_gitlab_source",
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "domain" = "domain",
 *     "group" = "group",
 *     "token" = "token",
 *   },
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "domain" = "domain",
 *     "group" = "group",
 *     "token" = "token",
 *   },
 *   links = {
 *     "add-form" = "/admin/config/development/project_browser_gitlab/source/add",
 *     "edit-form" = "/admin/config/development/project_browser_gitlab/source/{project_browser_gitlab_source}/edit",
 *     "delete-form" = "/admin/config/development/project_browser_gitlab/source/{project_browser_gitlab_source}/delete",
 *     "collection" = "/admin/config/development/project_browser_gitlab",
 *   }
 * )
 */
class GitlabSource extends ConfigEntityBase implements GitlabSourceInterface {

  /**
   * The ID of the gitlab source.
   *
   * @var int
   */
  protected $id;

  /**
   * The name of the source.
   *
   * @var string
   */
  protected $label;

  /**
   * The domain of the Gitlab instance.
   *
   * @var string
   */
  protected $domain;

  /**
   * The group to filter projects by.
   *
   * @var string
   */
  protected $group;

  /**
   * The token to authenticate with the Gitlab API.
   *
   * @var string
   */
  protected $token;

  /**
   * {@inheritdoc}
   */
  public function id() {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function label() {
    return $this->label;
  }

  /**
   * {@inheritdoc}
   */
  public function domain() {
    return $this->domain;
  }

  /**
   * {@inheritdoc}
   */
  public function group() {
    return $this->group;
  }

  /**
   * {@inheritdoc}
   */
  public function token() {
    return $this->token;
  }

}

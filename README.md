This is still work in progress. This README will be kept simple until a stable release is ready.

# Configuration
After the module is installed, visit /admin/config/development/project_browser_gitlab
to configure Gitlab sources.

# Future
Here are some ideas for the future:
- Make it possible to enforce some default filters to be send to the API.
- Integrate with package_manager
- Improve the information displayed in the project browser (changed, created, active, ...)
